const { default: axios } = require("axios");
const fs = require("fs");



function fetchAndSaveReadme(apiUrl, version, projectName) {
  return axios
    .get(apiUrl)
    .then((response) => {
      if (response.status === 200) {
        const folderPath = `./mddocs/${projectName}/${version}`;
        if (!fs.existsSync(folderPath)) {
          fs.mkdirSync(folderPath, { recursive: true });
        }
        const readmeContent = response.data;
        const frontmatter = 
        `---
version: ${version}
name: ${projectName}
---`
        const data =`${frontmatter}
        ${readmeContent}
        `
        fs.writeFile(
          `${folderPath}/${projectName}.md`,
          data,
          (err) => {
            if (err) {
              console.error(`Error writing ${projectName}:`, err);
            }
          }
        );
      } else {
        console.error(`Failed to fetch ${projectName}.`);
      }
    })
    .catch((error) => {
      console.error("Error:", error);
    });
}

module.exports = fetchAndSaveReadme;
