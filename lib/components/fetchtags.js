const axios = require("axios");

function fetchtags(apiUrlTags) {
  return axios
    .get(apiUrlTags)
    .then((response) => {
      const specificrelease = response.data[0];
      return specificrelease.name;
    })
    .catch((error) => {
      return "0.0.1";
    });
}
module.exports = fetchtags;
