function sleep(ms) {
  const localms = ms * 1000;
  return new Promise((resolve) => setTimeout(resolve, localms));
}

module.exports = sleep;
