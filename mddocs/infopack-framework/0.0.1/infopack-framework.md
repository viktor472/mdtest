---
version: 0.0.1
name: infopack-framework
---
        # Information Package Framework

This document defines the information package framework (infopack) which is an abstract standard for information sharing in an open source manner. It is community developed, it depends heavily on open source standards and it is published under a MIT license.

## Description

An infopack is essentially a storage container with a well defined structure and a small software attached. More exact a package contains some mandatory files and folders and some input data. The data is also wrapped with a software which generates a static output when needed (prefably upon version-based release cycles).

The embedded software is configurable and expandable and is responsible for generating the output data from the input.

![](./media/infopack-io.png)

A key benefit to use this very crude approach to data storage is the possibility to put the folder under version control with softwares like [GIT](https://git-scm.com/) and the fact that it is software agnostic.

### Ecosystem

The ecosystem is split into some core components with the goal to offer maximal flexibility for different use cases.

![An image describing the overall ecosystem](./media/infopack-ecosystem.png)

The physical storage and the GUI (registry) are separated, making it possible to host infopacks at different locations and index them in a single registry.

> **Please note!** This framework does primarily define the infopack and the interface between the other components. Developers should honor these rules when making own implementations of the storage and/or registry components.

The true magic to the whole infopack ecosystem is the web friendly nature of the output. Most likely the output from an infopack will be published on a static file server which is made public on the internet. This enables people and organizations to take advantage of each others data and create awesome things together!

## Definitions

Some common terminology is defined within the framework.

| Term                           | Definition                                      |
|--------------------------------|-------------------------------------------------|
| Information package (infopack) | A container of information that's intented to be processed to one or more publications with an extraction process. |
| Extraction process             | The process of extracting publications from a information package |
| Pipeline                       | A concept within the extraction process for structuring the operations (steps) that are run within an information package. |
| Step (in pipeline)             | A block of instructions which are executed by the pipeline. | 
| Generator                      | A generator is basically a predefinied step (it generates a step for the pipeline). |


## Content requirements

An implementation must follow some high level requirements as follows.

### Infopack definition file - infopack.json

Every infopack must have a definition file. It must be named `infopack.json` and must contain information about:

| Property          | Type   | Mandatory | Description |
|-------------------|--------|-----------|-------------|
| framework_version | String | Yes       | The version of the framework which the infopack complies with |
| implementation    | String | Yes       | Which implementation the package is built with |
| metadata          | Object | No        | key value pair of text with custom metadata | 

**Example**

```json
{
    "framework_version": "1.0.0",
    "implementation": "node",
    "metadata": { ... }
}
```

This file should also be pushed to the output folder on during run.

### Folder structure

An infopack should be structured in a consistent way. The primary reason for this is to provide a clear structure for the generators to operate on. Another reason is to have the possibility to use different implementations of the framework on the same information package.

| Folder name    | VC | Description | Typical file formats |
|----------------|----|-------------|----------------------|
| input          | X  | Source information is stored here. The information is typically text based "source control friendly" formats. Information can be fragmented into small pieces for manintainability and easy processing. | `yml`, `json`, `xml` |
| output         |    | The result of the pipeline should be put here | `pdf`, `docx`, `html (static website)`, `json` |
| cache          |    | Temporary data are being put here, this folder is reset for every run |  |

### Implementation files

Besides the files and folders mentioned above the infopack folder may contain implementation specific files (eg. package.json file in the node implementation).

## Extraction process

During the extraction process the following should happend

* Input data should be processed to output data via the pipeline
* JSON sidecar files for each output file should be generated
    * HTML sidecar files should be generated for each JSON sidecar files is standalone flag is applied
* Mandatory infopack files should be generated

The framework aims to leave as much freedom as possible to the implementations by not putting detailed requirements. Some basic concepts are defined that helps "harmonize" the implementations.

### Pipeline

TBD

### Generators

TBD

## Output structure

The extraction process results in an output. The output are structured in a way that it should be readable for both humans and computers. To achive this each file is accompanied with two "sidecar" files, see the example below.

Let say we have a file called `basic instruction.pdf` containing some basic instructions. In the same directory there should also be a file called `basic instruction.pdf.meta.json` and another one called `basic instruction.pdf.meta.html`. The first one is for computers or programs to parse and read and the second is for a human to read with the help of a web browser.

The implementation should generate this sidecar files during the extraction process.

### Mandatory output files

In the same manner, each output must contain a `index.html` and a `index.json`.

### Example

Below is an example of a folder structure for an infopack output containing one file called "basic instruction.pdf" along with the necessary files.

```
.
├── basic instruction.pdf
├── basic instruction.pdf.meta.html
├── basic instruction.pdf.meta.json
├── index.html
├── index.json
└── infopack.json
```

## Implementations

Implementations of the above standard are listed here.

* [NodeJS - infopack](https://gitlab.com/infopack/infopack)

## Read more

The official webpage are hosted on [https://infopack.io](https://infopack.io).

The official registry can be found over at [https://registry.infopack.io](https://registry.infopack.io).

        