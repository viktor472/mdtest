---
version: ci-test
name: infopack-common-schemas
---
        # Infopack common schemas

This repo contains all the common schemas for the infopack ecosystem. We are using the [JSON-schema standard](https://json-schema.org/).

All schemas are located in the `schemas` folder and they are published to [https://schemas.infopack.io/](https://schemas.infopack.io/) for your convenience.

## Schema philosophy

### Its okay to have verbose schemas  

Lets take [index.json schema](./schemas/infopack-index.2.schema.json) as an example. We have the property key `path` with the example value `some_folder/example.txt`. From this key a client can extract portions of the path if needed for specific use cases (such as extracting the file extension for displaying a symbol for a file). Since storing text is cheep we have decided to store the extractions as separate keys. The keys `dirname`, `filename` and `extname` are portions of the path key ready to use for clients.

| Key      | Value                   | Comment                          |
| -------- | ----------------------- | -------------------------------- |
| path     | some_folder/example.txt |                                  |
| dirname  | some_folder             | With nodeJS: path.dirname(path)  |
| filename | example.txt             | With nodeJS: path.basename(path) |
| extname  | .txt                    | With nodeJS: path.extname(path)  |

The same applies to the meta files. According to the standard all infopack files should have a corresponding `.meta.json` and an optional `.meta.html` file. The path to these to files are stored in the property keys `metaJsonPath` and `metaHtmlPath`.

## Use in other packages

A common use case would be to use this repo in situation where you would like to validate json data according to these schemas.

```
var schemas = require('infopack-common-schemas');
var schemaDef = schemas.get('someSchema');
```

        