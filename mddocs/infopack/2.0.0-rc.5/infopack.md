---
version: 2.0.0-rc.5
name: infopack
---
        # infopack - A NodeJS information package implementation

This package implements the [Information package framework](https://gitlab.com/infopack/infopack-framework). You can learn more about the framework at [infopack.io](http://www.infopack.io).

The infopack source is basically a small application that generates publications (infopack releases) out of the source information. It does this by creating and running "steps" via a pipeline. The steps manipulates the information package to create the needed outcome.

## Infopack source content structure

An implementation must follow some high level requirements as follows.

### Infopack definition file - infopack.json

Every infopack must have a definition file. It must be named `infopack.json` and must contain information about:

| Property          | Type   | Mandatory | Description |
|-------------------|--------|-----------|-------------|
| framework_version | String | Yes       | The version of the framework which the infopack complies with |
| implementation    | String | Yes       | Which implementation the package is built with |
| metadata          | Object | No        | key value pair of text with custom metadata | 

**Example**

```json
{
    "framework_version": "1.0.0",
    "implementation": "node",
    "metadata": { ... }
}
```

This file should also be pushed to the output folder on during run.

### Folder structure

An infopack should be structured in a consistent way. The primary reason for this is to provide a clear structure for the generators to operate on. Another reason is to have the possibility to use different implementations of the framework on the same information package.

| Folder name    | VC | Description | Typical file formats |
|----------------|----|-------------|----------------------|
| input          | X  | Source information is stored here. The information is typically text based "source control friendly" formats. Information can be fragmented into small pieces for manintainability and easy processing. | `yml`, `json`, `xml` |
| output         |    | The result of the pipeline should be put here | `pdf`, `docx`, `html (static website)`, `json` |
| cache          |    | Temporary data are being put here, this folder is reset for every run |  |

### Implementation files

Besides the files and folders mentioned above the infopack folder may contain implementation specific files (eg. package.json file in the node implementation).

## Extraction process

During the extraction process the following should happend

* Input data should be processed to output data via the pipeline
* JSON sidecar files for each output file should be generated
    * HTML sidecar files should be generated for each JSON sidecar files is standalone flag is applied
* Mandatory infopack files should be generated

The framework aims to leave as much freedom as possible to the implementations by not putting detailed requirements. Some basic concepts are defined that helps "harmonize" the implementations.

### Pipeline

TBD

### Generators

TBD

## Usage

Use npm to install the package in you app.

### Install package

```
$ npm install --save infopack
```

### In your app

```javascript
// ./index.js

// require package
import infopack from 'infopack'

// Create pipeline
var pipeline = new infopack.default([
	{
		run: function(executor) {
			console.log('Hello from step')
		}
	}
]);

// run pipeline
pipeline.run()
```

A cache folder and an output folder will be generated in the project root so it might be a good idea to ignore these folders in version control.

```
// ./.gitignore

node_modules/
cache/
output/
```

## Pipeline

The pipeline is constructed with one or more steps, a step is basically a function with its optional parameters, If async tasks are performed this function need to return a promise. A step can be hand written in your app or generated with a generator.

### Step definition

**Basic step**

```
{
	run: function(pipeline) {
		return Promise.resolve({ data: { ... } });
	}
}
```

**Advanced step**

```
{
	settings: { key: 'value' },
	run: function(pipeline, settings) {
		console.log(settings.key); // -> value
	}
}
```

### Run cycle

After a pipeline is created and populated with steps you simply run it by `pipeline.run()`. A cache folder will be created/reset with every run.

## API

### Pipeline

#### new Pipeline([inputSteps], [options]);

| Property                  | Type   | Definition                                                                         | Default                                                          | Example |
|---------------------------|--------|------------------------------------------------------------------------------------|------------------------------------------------------------------|---------|
| inputSteps                | step[] |                                                                                    | []                                                               |         |
| options                   | object | Optional options object                                                            | {}                                                               |         |
| options.basePath          | string | Declare base path for the infopack, this path will be resolved to an absolute path | path.resolve(options.basePath || '')                             |         |
| options.inputFolderPath   | string |                                                                                    | path.join(this.basePath, options.inputFolderName || 'input')     |         |
| options.outputFolderPath  | string |                                                                                    | path.join(this.basePath, options.outputFolderName || 'output')   |         |
| options.cacheFolderPath   | string |                                                                                    | path.join(this.basePath, options.cacheFolderName || 'cache')     |         |
| options.indexHtmlPath     | string |                                                                                    | path.join(__dirname, '..', 'templates', 'index.template.html')   |         |
| options.sidecarHtmlPath   | string |                                                                                    | path.join(__dirname, '..', 'templates', 'sidecar.template.html') |         |
| options.versionSuffix     | string | Optional value to append to version number derived from package.json file. Useful for appending vcs info to non release builds | undefined |         |

**Please note** Version suffix can also be added via commandline. eg `npm start -- <some_vcs_info>` would set version to <package_version>-<some_vcs_info>


#### pipeline.addStep(step)

Add a single step to the end of the pipeline.

#### pipeline.getBasePath()

Returns the base path for the infopack.

#### pipeline.getInputPath()

Returns the work directory path.

#### pipeline.getOutputPath()

Returns the output directory path.

#### pipeline.getCachePath()

Returns the cache directory path.

### Executor

The executor class is instantiated with every pipeline.run() and takes care of all the heavy lifting. 

### executor.toOutput(infopackContent: InfoPackContent)

| Property                  | Type            | Definition                                                                 | Default | Example                       |
|---------------------------|-----------------|----------------------------------------------------------------------------|---------|-------------------------------|
| fileContent               | InfopackContent |                                                                            |         |                               |
| fileContent.path          | string          | File path (local path in output folder)                                    |         | file.json or folder/file.json |
| fileContent.data          | Buffer          | File data                                                                  |         |                               |
| fileContent.title         | string          | File title                                                                 |         |                               |
| fileContent.description   | string          | File description                                                           |         |                               |
| [fileContent.labels]      | Object          | Optional key value pairs                                                   |         |                               |

## Generators

Generators are small applications that generates a step which can in the pipeline. Generators should be as generic as possible for compatibility purposes.

        