---
version: 0.8.3
name: infopack-cli
---
        # Information package CLI tool

A small CLI tool for the [Information package framework](https://gitlab.com/infopack/infopack-framework) [nodeJS implementation](https://gitlab.com/infopack/infopack).

## Setup

```
$ npm install -g infopack-cli
```

## Usage

use `infopack --help` to see all available commands.

**Example** - Setup new generator
```
$ infopack init infopack-gen-my-generator generator 
```

        