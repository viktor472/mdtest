---
version: 0.0.1
name: storage
---
        # Infopack Storage

## Dev

### Setup MariaDB server

```
docker run --detach --name infopack-storage-dev -p 3307:3306 --env MARIADB_DATABASE=infopack-storage --env MARIADB_USER=user --env MARIADB_PASSWORD=password --env MARIADB_ROOT_PASSWORD=my-secret-pw  mariadb:latest
```

### Use docker compose

```
docker compose up
```
Please note that compose >= 2 since we are using healthcheck on mariadb-service

## Config

This app uses https://www.npmjs.com/package/config for handling configuration. Please see docs for proper usage.

Config files are stored in `/server/config` and `/server/config/local.json` is in .gitignore file.

### Storage providers

The storage comes prepacked with two different storage providers:

* Local storage
* GCP Storage

#### Local storage

These are the relevant properties with its corresponding environment variable names

```
{
    ...
    "storageDriver": {
        "driver": "STORAGEDRIVER_DRIVER",
        "localStorage": {
            "localPath": "STORAGEDRIVER_LOCAL_LOCALPATH"
        }
    }
}
```

STORAGEDRIVER_DRIVER = local-storage

#### GCP Storage

These are the relevant properties with its corresponding environment variable names

```
{
    ...
    "storageDriver": {
        "driver": "STORAGEDRIVER_DRIVER",
        "gcpStorage": {
            "projectId": "STORAGEDRIVER_GCPSTORAGE_PROJECTID",
            "bucketName": "STORAGEDRIVER_GCPSTORAGE_BUCKET_NAME",
            "location": "STORAGEDRIVER_GCPSTORAGE_LOCATION",
            "credentials": "STORAGEDRIVER_GCPSTORAGE_CREDENTIALS"
        },
    }
}
```

STORAGEDRIVER_DRIVER = gcp-storage
        