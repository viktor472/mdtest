const data = require("./repos.json");
const fetchtags = require("./lib/components/fetchtags");
const fetchAndSaveReadme = require("./lib/components/fetchreadme");
const { version } = require("os");
const { resolve } = require("path");
const sleep = require("./lib/components/sleep");

/* https://gitlab.com/api/v4/projects/40552380/repository/tree?path=docs&recursive=true */

for (const repo of data) {
  const projectName = repo.projectName;
  const projectId = repo.projectId;
  const apiUrl = `https://gitlab.com/api/v4/projects/${projectId}/repository/files/README.md/raw`;
  const apiUrlTags = `https://gitlab.com/api/v4/projects/${projectId}/repository/tags`;
  var localversion = "";
  fetchtags(apiUrlTags)
    .then((version) => {
      localversion = version;
    })
    .then(() => {
      fetchAndSaveReadme(apiUrl, localversion, projectName);
      console.log("-------------");
      console.log("Saved:");
      console.log(`${projectName} : ${projectId}`);
      console.log(`V: ${localversion}`);
      console.log("-------------");
    })
    .catch((error) => {
      console.error("Error:", error);
    });
}
